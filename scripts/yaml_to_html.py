import os
import model
from jinja2 import Environment, FileSystemLoader


def mkdirs(dest: str):
    if not os.path.isdir(dest):
        os.makedirs(dest)


IS_PROD = os.getenv('CI_COMMIT_REF_NAME', 'master') == 'master'
ENV = 'prod' if IS_PROD else 'dev'


mkdirs('./html')
with open('./versions.txt', 'r') as f:
    versions = f.read()
versions = ['Latest'] + list(filter(len, versions.split('\n')))[1:6]


def main():
    env = Environment(loader=FileSystemLoader('./scripts/templates'))
    class_template = env.get_template('class_template.html')
    m = model.Model.load_yaml('./yaml')
    for t in m.types:
        if type(t) == model.ClassType:
            write_class(class_template, m, t)


def write_class(template, m, t):
    super_classes = m.get_super_classes(t)
    examples = get_example(t.examples)
    text = template.render(model=t, super_classes=super_classes, examples=examples, versions=versions, env=ENV)
    file_path = './html/%s.html' % t.name.replace(' ', '')
    with open(file_path, 'w') as f:
        f.write(text)


def get_example(examples):
    def read_example(example):
        path = './examples/' + example
        with open(path, 'r') as f:
            example = f.read()
        return example
    return list(map(read_example, examples))


if __name__ == '__main__':
    main()
