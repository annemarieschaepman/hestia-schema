const { writeFileSync } = require('fs');
const { join, resolve } = require('path');

const [name] = process.argv.slice(2);

if (!name) {
  console.error('Please provide a migration name.');
  process.exit(1);
}

const ROOT = resolve(join(__dirname, '..'));
const folder = join(ROOT, 'hestia_earth', 'schema', 'migrations');

const template = `
import pandas as pd


def migrate(df: pd.DataFrame):
    # migrate data

    # make sure to return the DataFrame so it can be uploaded
    return df
`.trimLeft();

const run = () => {
  const filename = `${new Date().getTime()}-${name}.py`.replace(/[\-\s]/g, '_');
  const filepath = join(folder, filename);
  writeFileSync(filepath, template, 'utf-8');
};

run();
