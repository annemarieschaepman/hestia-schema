# Graphical overview of the data structure

```mermaid
graph LR;
classDef className fill:#FFBF00;

ImpactAssessment --> Cycle

Cycle --> Site & Source & Completeness & Input & Emission & Product;

Bibliography --> Actor;

Site --> Organisation & Measurement & Infrastructure;

Source --> Bibliography & Actor;

Infrastructure --> Source & Input;

Emission & Measurement & Input & Product --> Source;

Input --> ImpactAssessment

class ImpactAssessment,Cycle,Site,Source,Bibliography,Actor,Organisation className;
```

#### Key

```mermaid
graph LR
    Node:::className
    classDef className fill:#FFBF00;
```

```mermaid
graph LR
    id1["Blank Node"]
```

#### Notes

This diagram does not include Terms (which are nodes) or Properties (which are blank nodes). These are used to enforce the controlled vocabulary and can be added to almost any part of the data structure.
