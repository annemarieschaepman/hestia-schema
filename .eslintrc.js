module.exports = {
  env: {
    node: true
  },
  extends: [
    '@hestia-earth/eslint-config'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module'
  },
  "rules": {
    "complexity": "off",
    "@typescript-eslint/no-shadow": "off",
    "no-console": "off"
  }
};
