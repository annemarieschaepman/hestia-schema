class:
  name: Infrastructure
  type: Blank Node
  examples:
    - infrastructure.jsonld
  doc: >
    A physical item on a [Site]. Default data for the [Inputs](./Input)
    required to create the Infrastructure are available by selecting a type of
    Infrastructure from the [Glossary of Terms](/glossary). Infrastructure is
    amortized over [Cycles](./Cycle) and the becomes an [Input] and terms for this
    are available in the [Glossary](/glossary?termType=material).

  properties:
    - name: term
      type: Ref[Term]
      doc: >
        A reference to the [Term] describing the Infrastructure. This can be used to
        pull across default [Inputs](./Input) and lifespans which are defined in the Term.
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: name
      type: string
      doc: A name for the Infrastructure, if not described by a Term.
      exportDefaultCSV: true

    - name: description
      type: string
      doc: A short description of the Infrastructure.
      exportDefaultCSV: true

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        The date when the Infrastructure was built
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD, YYYY-MM,
        or YYYY).
      exportDefaultCSV: true

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      doc: >
        The (expected) date when the Infrastructure was (will be) dismantled
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD, YYYY-MM,
        or YYYY).
      exportDefaultCSV: true

    - name: lifespan
      type: number
      exclusiveMinimum: 0
      doc: >
        The (expected) lifespan, expressed in decimal years.
        Equal to <code>[endDate](#endDate) - [startDate](#startDate)</code>.
      exportDefaultCSV: true

    - name: lifespanHours
      type: number
      exclusiveMinimum: 0
      doc: The (expected) lifespan, expressed in hours.
      exportDefaultCSV: true

    - name: mass
      type: number
      exclusiveMinimum: 0
      doc: The mass of the Infrastructure, expressed in kilograms.
      exportDefaultCSV: true
      recommended: true

    - name: area
      type: number
      exclusiveMinimum: 0
      doc: The area of the Infrastructure, expressed in hectares.
      exportDefaultCSV: true

    - name: ownershipStatus
      type: string
      enum:
        - owned
        - rented
        - borrowed
      doc: The ownership status of the Infrastructure.
      exportDefaultCSV: true

    - name: source
      type: Ref[Source]
      doc: A reference to the [Source] of these data, if different from the Source of the Site.
      exportDefaultCSV: true

    - name: impactAssessment
      type: Ref[ImpactAssessment]
      doc: >
        A reference to the node containing environmental impact data related to
        producing this Infrastructure.
      exportDefaultCSV: true

    - name: inputs
      type: List[Input]
      doc: The [Inputs](./Input) required to create the Infrastructure.
      exportDefaultCSV: true

    - name: transport
      type: List[Transport]
      doc: A list of [Transport](./Transport) stages to bring this Product to the [Cycle].
      exportDefaultCSV: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - building
                    - cropSupport
                    - irrigation
                    - machinery
      - if:
          required:
            - inputs
        then:
          properties:
            inputs:
              items:
                properties:
                  term:
                    properties:
                      termType:
                        enum:
                          - electricity
                          - fuel
                          - material
                          - other
                          - transport
                          - water
      - if:
          required:
            - inputs
          properties:
            inputs:
              items:
                properties:
                  term:
                    properties:
                      termType:
                        const: material
        then:
          properties:
            inputs:
              items:
                properties:
                  term:
                    properties:
                      units:
                        const: kg
      - anyOf:
        - required:
          - term
        - required:
          - name
        - required:
          - description
