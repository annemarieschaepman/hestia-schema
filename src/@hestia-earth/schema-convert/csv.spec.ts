import { expect } from 'chai';
import 'mocha';

import { toCsv } from './csv';

describe('schema-convert/csv', () => {
  describe('toCsv', () => {
    const boundary = { type: 'Feature', geometry: { type: 'Polygon' } };
    const nodes = [{
      type: 'Cycle',
      id: '1',
      description: 'first desc',
      defaultSource: {
        id: '1'
      },
      inputs: [{
        term: {
          id: 'genericSeed'
        },
        value: [
          0,
          234
        ]
      }],
      cycleDuration: null
    }, {
      type: 'Cycle',
      id: '2',
      description: 'second, cycle',
      defaultSource: {
        type: 'Source',
        id: '1'
      },
      inputs: [{
        term: {
          id: 'diesel'
        },
        value: [
          200,
          'Rice, grain'
        ]
      }]
    }, {
      '@type': 'Actor',
      '@id': 'actor-1',
      name: 'First Last'
    }, {
      type: 'ImpactAssessment',
      id: '1',
      description: 'my impact'
    }];

    it('should filter by headers', () => {
      expect(toCsv(nodes, false, false, [
        'cycle.id', 'cycle.defaultSource.id', 'cycle.inputs.0.value'
      ]).split('\n')).to.deep.equal([
        'cycle.id,cycle.defaultSource.id,cycle.inputs.0.value',
        '1,1,0;234',
        '2,1,"200;Rice, grain"',
        '-,-,-'
      ]);
    });

    it('should handle GeoJSON as string', () => {
      const node = {
        type: 'Organisation',
        boundary: JSON.stringify(boundary)
      };

      expect(toCsv([node]).split('\n')).to.deep.equal([
        'organisation.boundary',
        '"{""type"":""Feature"",""geometry"":{""type"":""Polygon""}}"'
      ]);
    });

    it('should handle GeoJSON as object', () => {
      const node = {
        type: 'Organisation',
        boundary
      };

      expect(toCsv([node]).split('\n')).to.deep.equal([
        'organisation.boundary',
        '"{""type"":""Feature"",""geometry"":{""type"":""Polygon""}}"'
      ]);
    });

    it('should convert dates', () => {
      const node = {
        type: 'Actor',
        uploadDate: new Date(2010, 0, 1)
      };
      const [header, row1] = toCsv([node]).split('\n');
      expect(header).to.equal('actor.uploadDate');
      expect(row1).to.match(/^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?/);
    });

    describe('including existing nodes', () => {
      it('should return csv as text', () => {
        expect(toCsv(nodes).split('\n')).to.deep.equal([
          'cycle.id,cycle.description,cycle.defaultSource.id,cycle.inputs.0.term.id,cycle.inputs.0.value,' +
          'cycle.cycleDuration,actor.@id,actor.name,impactAssessment.id,impactAssessment.description',
          '1,first desc,1,genericSeed,0;234,-,-,-,-,-',
          '2,"second, cycle",1,diesel,"200;Rice, grain",-,-,-,-,-',
          '-,-,-,-,-,-,actor-1,First Last,-,-',
          '-,-,-,-,-,-,-,-,1,my impact'
        ]);
      });
    });

    describe('non-including existing nodes', () => {
      it('should return csv as text', () => {
        expect(toCsv(nodes, false, true).split('\n')).to.deep.equal([
          'cycle.id,cycle.description,cycle.defaultSource.id,cycle.inputs[0].term.id,cycle.inputs[0].value,' +
          'cycle.cycleDuration,impactAssessment.id,impactAssessment.description',
          '1,first desc,1,genericSeed,0;234,-,-,-',
          '2,"second, cycle",1,diesel,"200;Rice, grain",-,-,-',
          '-,-,-,-,-,-,1,my impact'
        ]);
      });
    });
  });
});
