import { JSON, SchemaType, NodeType } from '../types';

export const isTypeNode = (type: SchemaType | NodeType) => Object.values(NodeType).includes(type as NodeType);

export const isNode = <T extends SchemaType>({ type }: Partial<JSON<T>>) => isTypeNode(type);

export const jsonldPath = (type: SchemaType | NodeType, id: string) => `${type}/${id}.jsonld`;

export const isTypeValid = <T extends SchemaType>({ type }: Partial<JSON<T>>) =>
  Object.values(SchemaType).includes(type);

export const isBlankNode = (node: any) => {
  const type = node['@type'] || node.type;
  return isTypeValid({ type }) && !isTypeNode(type);
};

export const isExpandable = (val: any) => !!val && !(val instanceof Date) && typeof val === 'object' && (
  Array.isArray(val) ? val.every(isExpandable) : Object.values(SchemaType).includes(val.type || val['@type'])
);

export const typeToSchemaType = (type?: string) =>
  Object.values(SchemaType).find(val => val.toLowerCase() === (type || '').toLowerCase());

export const refToSchemaType = (ref = ''): string | SchemaType =>
  ref ? (
    ref.startsWith('http') ?
      ref :
      typeToSchemaType(ref.substring(2).replace('-deep', '').replace('.json#', ''))
  ) : undefined;
