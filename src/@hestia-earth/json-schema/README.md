# Hestia JSON Schema

Hestia JSON Schema definitions, used to validate JSON files following hestia schmea.

## Install

```sh
npm install @hestia-earth/json-schema
```
