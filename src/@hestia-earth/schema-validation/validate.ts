import * as Ajv from 'ajv';
import { SchemaType, JSON as HestiaJson, isTypeNode } from '@hestia-earth/schema';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

const geojsonSchema = 'http://json.schemastore.org/geojson';
const existingNodeRequired = Object.freeze({ required: ['@id', '@type'] });

const validateSchemaType = (schemas: definitions, content) => {
  const type = content['@type'] || content.type;
  if (!(type in schemas)) {
    throw new Error(`Unknown or invalid type "${type}"`);
  }
  return true;
};

/**
 * Validate a single node. Function is asynchronous to return a list of `success` and a list of `errors`.
 *
 * @param ajv The AJV object. Use `initAjv()` or pass your own.
 * @param schemas The schema definitions. Use `loadSchemas` from `@hestia-earth/json-schema`  or pass your own.
 * @returns
 */
export const validateContent = (ajv: Ajv.Ajv, schemas: definitions) => async (
  content: HestiaJson<SchemaType>
) => validateSchemaType(schemas, content) && ({
  success: await ajv.validate(schemas[content['@type'] || content.type], content),
  errors: ajv.errors || []
});

const getByKey = (prev, curr) => !curr || !prev ? prev : (
  Array.isArray(prev) ? prev.map(item => getItemValue(item, curr)) : (
    prev ? prev[curr] : undefined
  )
);

const getItemValue = (item: any, key: string) =>
  key.split('.').reduce(getByKey, item);

const uniqueArrayItemKeyword = 'uniqueArrayItem';

function uniqueArrayItemValidate(keys: string[], items: any[], _schema?, dataPath = '') {
  const values = items.map(item => JSON.stringify(keys.reduce((prev, key) => ({
    ...prev, [key]: getItemValue(item, key)
  }), {})));
  const indexes = values
    .map((value, index) => values.findIndex((otherValue, otherValueIndex) =>
      otherValueIndex !== index && otherValue === value
    ))
    .filter(index => index >= 0)
    .sort();

  (uniqueArrayItemValidate as any).errors = indexes.map(index => ({
    dataPath: `${dataPath}[${index}]`,
    keyword: uniqueArrayItemKeyword,
    message: 'every item in the list should be unique',
    params: { keyword: uniqueArrayItemKeyword, keys }
  }));
  return indexes.length === 0;
}

const arraySameSizeKeyword = 'arraySameSize';

function arraySameSizeValidate(keys: string[], items: any[], _schema, dataPath, item: any) {
  const current = items.length;
  const errors = keys
    .filter(key => key in item)
    .map(key => {
      const expected = item[key].length;
      return current !== expected ? {
        dataPath,
        keyword: arraySameSizeKeyword,
        message: `must contain as many items as ${key}s`,
        params: { keyword: arraySameSizeKeyword, current, expected }
      } : null;
    })
    .filter(Boolean);
  (arraySameSizeValidate as any).errors = errors;
  return errors.length === 0;
}

export { uniqueArrayItemValidate, arraySameSizeValidate };

export const initAjv = () => {
  const ajv = new Ajv({
    schemaId: 'auto',
    allErrors: true
  });
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));
  ajv.addSchema(require('./json-schema/geojson.json'), geojsonSchema);

  ajv.addKeyword(uniqueArrayItemKeyword, {
    type: 'array',
    errors: 'full',
    validate: uniqueArrayItemValidate
  });

  ajv.addKeyword(arraySameSizeKeyword, {
    type: 'array',
    errors: 'full',
    validate: arraySameSizeValidate
  });
  return ajv;
};

/**
 * Create a validator instance. Call with a Node to validate it.
 *
 * @param domain The domain of the validator (defaults to Hestia's website)
 * @param strictMode Allow validating non-existing nodes, i.e. without unique `@id`.
 * @returns Function to validate a Node. Use `await validator()(node)`
 */
export const validator = (domain = 'https://www.hestia.earth', strictMode = true) => {
  const ajv = initAjv();
  const schemas = loadSchemas();

  Object.keys(schemas).map(schemaName => {
    const schema = schemas[schemaName];

    schema.properties = {
      '@context': {
        type: 'string'
      },
      ...schema.properties
    };
    schema.oneOf = strictMode && isTypeNode(schemaName as any) ? [existingNodeRequired] : schema.oneOf;
    ajv.addSchema(schema, `${domain}/schema/json-schema/${schemaName}#`);
  });
  return validateContent(ajv, schemas);
};
