import { expect } from 'chai';
import 'mocha';
import { readFileSync, readdirSync } from 'fs';
import { join } from 'path';

import { uniqueArrayItemValidate, arraySameSizeValidate, validator } from './validate';

describe('schema-validation/validate', () => {
  describe('uniqueArrayItemValidate', () => {
    const keys = ['term.@id', 'description'];

    describe('with valid items', () => {
      const items = [
        {
          term: {
            '@id': 'term-1'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          }
        },
        {
          term: {
            '@id': 'term-3'
          },
          description: 'test'
        }
      ];

      it('should return true', () => {
        expect(uniqueArrayItemValidate(keys, items)).to.equal(true);
      });
    });

    describe('with invalid items', () => {
      const items = [
        {
          term: {
            '@id': 'term-1'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          },
          description: 'test'
        },
        {
          term: {
            '@id': 'term-2'
          },
          description: 'test',
          value: 10
        }
      ];

      it('should return false', () => {
        expect(uniqueArrayItemValidate(keys, items)).to.equal(false);
      });
    });

    describe('with arrays', () => {
      const items = [
        {
          key: 'key1',
          array: [
            'value1',
            'value2'
          ],
          unique: true
        },
        {
          key: 'key1',
          array: [
            'value1',
            'value2'
          ],
          unique: false
        }
      ];

      it('should validate', () => {
        expect(uniqueArrayItemValidate(['array', 'key'], items)).to.equal(false);
        expect(uniqueArrayItemValidate(['key', 'unique'], items)).to.equal(true);
      });
    });
  });

  describe('arraySameSizeValidate', () => {
    const dataPath = '.key';
    const keys = ['value'];
    const items = [0, 10, 250];

    describe('with valid item', () => {
      const item = {
        value: [
          1, 20, 30
        ]
      };

      it('should return true', () => {
        expect(arraySameSizeValidate(keys, items, null, dataPath, item)).to.equal(true);
      });
    });

    describe('with invalid items', () => {
      const item = {
        value: [
          1, 20
        ]
      };

      it('should return false', () => {
        expect(arraySameSizeValidate(keys, items, null, dataPath, item)).to.equal(false);
      });
    });
  });

  describe('validator', () => {
    const folder = join(__dirname, '..', '..', '..', 'examples');
    const files = readdirSync(folder).filter(v => v.endsWith('.jsonld'));
    const validate = validator();

    files.map(file => {
      describe(file, () => {
        it('should validate', async () => {
          const data = JSON.parse(readFileSync(join(folder, file),  'utf8'));
          const result = await validate(data);
          expect(result.success).to.equal(true);
        });
      });
    });

    describe('unknown type', () => {
      const data: any = {
        type: 'Unknown'
      };

      it('should throw an error', async () => {
        try {
          await validate(data);
          expect(true).to.equal(false);
        }
        catch (err) {
          expect(err.message).to.equal('Unknown or invalid type "Unknown"');
        }
      });
    });
  });
});
