# Hestia Schema Validation

Module to validate data using the Hestia Schema and Ajv

## Install

```sh
npm install @hestia-earth/schema-validation
```
